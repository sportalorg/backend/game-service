package com.sportal.gameservice.game.integration;

import com.sportal.gameservice.game.dto.GameTemplateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SportsGroupIntegration {

    @Value("${variables.sportsgroup-service.base-url}")
    private String sportsGroupServiceBaseUrl;

    private final RestTemplate restTemplate;

    public Boolean organiserIsAdminCheck(String groupId, String accessToken){

        String url = sportsGroupServiceBaseUrl + "/sportsgroup/{groupId}/admincheck";

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", accessToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, groupId);

        return response.getStatusCode().is2xxSuccessful();
    }

    public GameTemplateDto getSportsGroupTemplate(String groupId){

        String url = sportsGroupServiceBaseUrl + "/sportsgroup/{groupId}/gametemplate";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<GameTemplateDto> response =
                restTemplate.exchange(url, HttpMethod.GET, entity, GameTemplateDto.class, groupId);
        return response.getBody();
    }

    public String[] getSportsGroupIdsFromSubscriptions(String accessToken){

        String url = sportsGroupServiceBaseUrl + "/sportsgroup/mysubscriptions/groupids";

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", accessToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, String[].class);

        return response.getBody();
    }
}
