package com.sportal.gameservice.game.repository;

import com.sportal.gameservice.game.entity.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends MongoRepository<Game, String> {

    @Query("{'players.athleteId': ?0}")
    List<Game> findGameByPlayer(String athleteId);

    @Query("{'groupId': ?0}")
    List<Game> findGamesByGroupId(String groupId);
}
