package com.sportal.gameservice.game.dto;

import com.sportal.gameservice.game.entity.Player;
import com.sportal.gameservice.game.entity.Sports;
import lombok.Builder;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.Set;

@Builder
public record GameDetailsResponse(
        String gameId,
        String groupId,
        Long teamSize,
        Sports sport,
        LocalDateTime dateTime,
        DayOfWeek dayOfWeek,
        Set<Player> players) {
}
