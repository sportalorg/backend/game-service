package com.sportal.gameservice.game.dto;

import com.sportal.gameservice.game.entity.Sports;
import lombok.Builder;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Builder
public record GameTemplateDto(
        String groupId,
        Long teamSize,
        Sports sport,
        DayOfWeek day,
        LocalTime time
) { }
