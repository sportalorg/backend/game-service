package com.sportal.gameservice.game.dto;

import com.sportal.gameservice.game.entity.Sports;

import java.time.LocalDateTime;

public record NewGameResponse(
        String groupId,
        Long teamSize,
        Sports sport,
        LocalDateTime dateTime) {
}
