package com.sportal.gameservice.game.controller;

import com.sportal.gameservice.game.dto.GameDetailsResponse;
import com.sportal.gameservice.game.dto.NewGameRequest;
import com.sportal.gameservice.game.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GameController {

    private final GameService gameService;

    @GetMapping("/game/{gameId}")
    public ResponseEntity<?> getGameById(
            @PathVariable String gameId){
        GameDetailsResponse response = gameService.getGameDetails(gameId);
        if(response != null){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/game")
    public ResponseEntity<?> createNewGame(
            @RequestHeader(name = "Authorization") String accessToken,
            @RequestBody NewGameRequest newGameRequest) {

        return gameService.createNewGame(newGameRequest, accessToken);
    }

    @PutMapping("/game/{gameId}/optin")
    public ResponseEntity<?> optInToPlay(
            @RequestHeader(name = "Authorization") String accessToken,
            @PathVariable String gameId) {

        final var response = gameService.optInToPlay(gameId,accessToken);

        if(response != null){
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PutMapping("/game/{gameId}/optout")
    public ResponseEntity<?> optOutToPlay(
            @RequestHeader(name = "Authorization") String accessToken,
            @PathVariable String gameId) {

        final var response = gameService.optOutToPlay(gameId,accessToken);

        if(response != null){
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @GetMapping("/game/upcominggames")
    public ResponseEntity<?> getUpcomingGames(
            @RequestHeader(name = "Authorization") String accessToken){

        List<GameDetailsResponse> response =
                gameService.getUpcomingGames(accessToken);

        if(response != null){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/game/upcominggames/optedin")
    public ResponseEntity<?> getUpcomingGamesWhereOptedIn(
            @RequestHeader(name = "Authorization") String accessToken){

        List<GameDetailsResponse> response =
                gameService.getUpcomingGamesWhereOptedIn(accessToken);

        if(response != null){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
