package com.sportal.gameservice.game.service;

import com.sportal.gameservice.game.dto.GameDetailsResponse;
import com.sportal.gameservice.game.dto.NewGameRequest;
import com.sportal.gameservice.game.dto.GameTemplateDto;
import com.sportal.gameservice.game.entity.Game;
import com.sportal.gameservice.game.entity.Player;
import com.sportal.gameservice.game.entity.PlayerStatus;
import com.sportal.gameservice.game.integration.SportsGroupIntegration;
import com.sportal.gameservice.game.repository.GameRepository;
import com.sportal.gameservice.utils.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class GameService {

    private final AuthUtils authUtils;
    private final SportsGroupIntegration sportsGroupIntegration;
    private final GameRepository gameRepository;

    public ResponseEntity<?> createNewGame(NewGameRequest newGameRequest, String accessToken) {

        Boolean isAdmin = sportsGroupIntegration.organiserIsAdminCheck(newGameRequest.groupId(), accessToken);


        if(!isAdmin){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        String organiserId = authUtils.getField(accessToken, "sub");

        if(newGameRequest.dateTime() != null) {
            Game newGame = Game.builder()
                    .groupId(newGameRequest.groupId())
                    .organiserId(organiserId)
                    .teamSize(newGameRequest.teamSize())
                    .sport(newGameRequest.sport())
                    .day(newGameRequest.dateTime().getDayOfWeek())
                    .dateTime(newGameRequest.dateTime())
                    .players(new HashSet<>())
                    .build();

            return new ResponseEntity<>(gameRepository.save(newGame), HttpStatus.CREATED);
        }else {
            GameTemplateDto gameTemplate =
                    sportsGroupIntegration.getSportsGroupTemplate(newGameRequest.groupId());
            LocalDateTime gameDateTime = LocalDateTime.now()
                    .with(TemporalAdjusters.next(gameTemplate.day()))
                    .withHour(gameTemplate.time().getHour())
                    .withMinute(gameTemplate.time().getMinute())
                    .withSecond(0)
                    .withNano(0);

            Game newGame = Game.builder()
                    .groupId(newGameRequest.groupId())
                    .organiserId(organiserId)
                    .teamSize(gameTemplate.teamSize())
                    .sport(gameTemplate.sport())
                    .day(gameDateTime.getDayOfWeek())
                    .dateTime(gameDateTime)
                    .players(new HashSet<>())
                    .build();

            return new ResponseEntity<>(gameRepository.save(newGame), HttpStatus.CREATED);
        }
    }

    public GameDetailsResponse optInToPlay(String gameId, String accessToken){

        String sub = authUtils.getField(accessToken, "sub");

        Optional<Game> optGame = gameRepository.findById(gameId);

        if(optGame.isPresent()){

            Game game = optGame.get();

            if(game.getPlayers().stream().filter(p -> p.getAthleteId().equals(sub)).toList().isEmpty()) {

                Player newPlayer = Player.builder()
                        .athleteId(sub)
                        .status(PlayerStatus.PENDING)
                        .optInDateTime(LocalDateTime.now())
                        .build();
                game.getPlayers().add(newPlayer);

                Game response = gameRepository.save(game);

                return GameDetailsResponse.builder()
                        .gameId(response.getId())
                        .groupId(response.getGroupId())
                        .teamSize(response.getTeamSize())
                        .sport(response.getSport())
                        .dateTime(response.getDateTime())
                        .dayOfWeek(response.getDay())
                        .players(response.getPlayers())
                        .build();
            }
        }
        return null;
    }
    public GameDetailsResponse optOutToPlay(String gameId, String accessToken){

        String sub = authUtils.getField(accessToken, "sub");

        Optional<Game> optGame = gameRepository.findById(gameId);

        if(optGame.isPresent()){

            Game game = optGame.get();
            Predicate<Player> isQualified = player -> player.getAthleteId().equals(sub);
            game.getPlayers().removeIf(isQualified);
            Game response = gameRepository.save(game);
            return GameDetailsResponse.builder()
                    .gameId(response.getId())
                    .groupId(response.getGroupId())
                    .teamSize(response.getTeamSize())
                    .sport(response.getSport())
                    .dateTime(response.getDateTime())
                    .dayOfWeek(response.getDay())
                    .players(response.getPlayers())
                    .build();
        }
        return null;
    }

    public GameDetailsResponse getGameDetails(String gameId){

        Optional<Game> optGame = gameRepository.findById(gameId);

        return optGame.map(game -> GameDetailsResponse.builder()
                .gameId(game.getId())
                .groupId(game.getGroupId())
                .teamSize(game.getTeamSize())
                .sport(game.getSport())
                .dateTime(game.getDateTime())
                .dayOfWeek(game.getDay())
                .players(game.getPlayers())
                .build()).orElse(null);
    }

    public List<GameDetailsResponse> getUpcomingGames(String accessToken){

        List<String> groupIds =
                Arrays.stream(sportsGroupIntegration.getSportsGroupIdsFromSubscriptions(accessToken)).toList();

        List<Game> games = new ArrayList<>();
        groupIds.forEach(id -> games.addAll(gameRepository.findGamesByGroupId(id)));

        return games.stream()
                .filter(game -> game.getDateTime().isAfter(LocalDateTime.now()))
                .map(game -> GameDetailsResponse.builder()
                        .gameId(game.getId())
                        .groupId(game.getGroupId())
                        .teamSize(game.getTeamSize())
                        .sport(game.getSport())
                        .dateTime(game.getDateTime())
                        .dayOfWeek(game.getDay())
                        .players(game.getPlayers())
                        .build())
                .collect(Collectors.toList());

    }

    public List<GameDetailsResponse> getUpcomingGamesWhereOptedIn(String accessToken){
        String sub = authUtils.getField(accessToken, "sub");

        return gameRepository.findGameByPlayer(sub).stream()
                .filter(game -> game.getDateTime().isAfter(LocalDateTime.now()))
                .map(game -> GameDetailsResponse.builder()
                        .gameId(game.getId())
                        .groupId(game.getGroupId())
                        .teamSize(game.getTeamSize())
                        .sport(game.getSport())
                        .dateTime(game.getDateTime())
                        .players(game.getPlayers())
                        .build())
                .collect(Collectors.toList());
    }
}

