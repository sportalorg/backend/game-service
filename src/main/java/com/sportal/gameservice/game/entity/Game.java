package com.sportal.gameservice.game.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@Document(collection = "games")
public class Game {

    @Id
    private String id;
    private String groupId;
    private String organiserId;
    private Long teamSize;
    private Sports sport;
    private DayOfWeek day;
    private LocalDateTime dateTime;
    private Set<Player> players; // this needs to be changed to a set as should be unique list of players

}
