package com.sportal.gameservice.game.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Player {
    private String athleteId;
    private PlayerStatus status;
    private LocalDateTime optInDateTime;
}
