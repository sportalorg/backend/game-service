package com.sportal.gameservice.game.entity;

public enum PlayerStatus {
    PENDING,
    PLAYING,
    SUBSTITUTE
}
